﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using YooTask.Model;
using System.Net;

namespace YooTask
{
    [Activity(Label = "Activity2")]
    public class RegiestrationActivity : Activity
    {
        EditText EmailEditText;
        EditText HasloEditText;
        EditText ImieEditText;
        EditText nazwiskoEditText;
        EditText TelefonEditText;
        EditText Haslo2EditText;
        EditText City, Company;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Rejestracja);

            var buttonRegiestr = FindViewById<Button>(Resource.Id.buttonRegiestr);

            buttonRegiestr.Click += ButtonRegiestr_Click;


            // Create your application here
        }

        private async void ButtonRegiestr_Click(object sender, EventArgs e)
        {
            EmailEditText = FindViewById<EditText>(Resource.Id.edtEmail);
            HasloEditText = FindViewById<EditText>(Resource.Id.edtPassword);
            ImieEditText = FindViewById<EditText>(Resource.Id.edtFirstName);
            nazwiskoEditText = FindViewById<EditText>(Resource.Id.edtSecondName);
            TelefonEditText = FindViewById<EditText>(Resource.Id.edtphoneNum);
            Haslo2EditText = FindViewById<EditText>(Resource.Id.edtPassword2);
            City = FindViewById<EditText>(Resource.Id.edtCity);
            Company = FindViewById<EditText>(Resource.Id.edtCompany);


            if (Isvalidated())
            {
                Regiestr regiestr = new Regiestr();
                
                
                regiestr.name = ImieEditText.Text;
                regiestr.password = HasloEditText.Text;
                regiestr.phoneNumber = TelefonEditText.Text;
                regiestr.secondname = nazwiskoEditText.Text;
                regiestr.city = City.Text;
                regiestr.company = Company.Text;
                regiestr.email = EmailEditText.Text;

                
                var json = JsonConvert.SerializeObject(regiestr);
                
                var request = new HttpRequestMessage();
                HttpContent httpContent = new StringContent(json);
                //var value = new MediaTypeWithQualityHeaderValue("application/json");
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var client = new HttpClient();
                var response = await client.PostAsync("http://192.168.134.66:8081/user/signup", httpContent);

               // request.Headers.Accept.Add(value);
                // var uri = new Uri(string.Format($"https://192.168.134.66:8081/project/regiestr?email=" + EmailEditText.Text + "&password=" + HasloEditText.Text + "&first=" + ImieEditText.Text + "&second=" + nazwiskoEditText.Text
                //    + "&city=" + City.Text + "&company=" + Company.Text + "&phoneNumber=" + TelefonEditText.Text));
             /*   request.RequestUri = new Uri($"http://192.168.134.66:8081/user/signup?email=" + EmailEditText.Text + "&password=" + HasloEditText.Text + "&firstName=" + ImieEditText.Text
                       + "&surname=" + nazwiskoEditText.Text + "&city=" + City.Text + "&company=" + Company.Text + "&phoneNumber=" + TelefonEditText.Text);
                request.Method = HttpMethod.Post;
                request.Headers.Add("Accept", "application/json");
                var client = new HttpClient();
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(request.RequestUri, content);
                */
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var token = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                    {
                '"'
                    

                });
                    Toast.MakeText(this, token, ToastLength.Long).Show();
                    //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    Intent nextActivity = new Intent(this, typeof(Menu));
                    var token1 = JsonConvert.DeserializeObject(token);
                    StartActivity(nextActivity);
                }
                else
                {
                    var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                    {
                '"'
                    });
                    Toast.MakeText(this, errorMessage1, ToastLength.Long).Show();
                    Intent nextActivity = new Intent(this, typeof(Menu));
                  //  var token1 = JsonConvert.DeserializeObject(errorMessage1);
                    StartActivity(nextActivity);
                }
                
                
              //  HttpContent content2 = response.Content;
               // var json2 = await content.ReadAsStringAsync();
               //response deserialize 
    



            }
        }

            bool Isvalidated()
            {
                EmailEditText = FindViewById<EditText>(Resource.Id.edtEmail);
                HasloEditText = FindViewById<EditText>(Resource.Id.edtPassword);
                ImieEditText = FindViewById<EditText>(Resource.Id.edtFirstName);
                nazwiskoEditText = FindViewById<EditText>(Resource.Id.edtSecondName);
                TelefonEditText = FindViewById<EditText>(Resource.Id.edtphoneNum);
                Haslo2EditText = FindViewById<EditText>(Resource.Id.edtPassword2);



                if ((EmailEditText.Text == "") || (HasloEditText.Text == "") || (ImieEditText.Text == "") || (nazwiskoEditText.Text == ""))
                {
                    ShowMessage("Błąd", "Pola Imie, Nazwisko, Hasło, Email są wymagane");
                    return false;
                }


                if (TelefonEditText.Text == "")
                {

                }
                else if (TelefonEditText.Text.Length != 9)
                {
                    ShowMessage("Błąd", "Niepoprawna długość numeru telefonu");
                    return false;
                }

                string telefon;
                int licznikk = 0;
                telefon = TelefonEditText.Text;

                for (int i = 0; i < TelefonEditText.Text.Length; i++)
                {
                    if ((telefon[i] != '0') && (telefon[i] != '1') && (telefon[i] != '2') && (telefon[i] != '3') && (telefon[i] != '4') && (telefon[i] != '5') && (telefon[i] != '6') && (telefon[i] != '7') && (telefon[i] != '8') && (telefon[i] != '9'))
                    {
                        licznikk = licznikk + 1;
                    }

                }

                if (licznikk != 0)
                {
                    ShowMessage("Błąd", "Telefon może składać się tylko z liczb");
                    return false;
                }


                string Email;
                Email = EmailEditText.Text;
                int licznik = 0;

                for (int i = 0; i < EmailEditText.Text.Length; i++)
                {
                    if (Email[i] == '@')
                    {
                        licznik = licznik + 1;
                    }

                }

                if (licznik != 1)
                {
                    ShowMessage("Błąd", "Niepoprawny adres Email");
                    return false;
                }

                string haslo;
                string haslo2;
                haslo = HasloEditText.Text;
                haslo2 = Haslo2EditText.Text;

                if (haslo != haslo2)
                {
                    ShowMessage("Błąd", "Hasła są różne");
                    return false;
                }



                return true;
            }

           private void ShowMessage(string title, string message)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                AlertDialog alertDialog = builder.Create();
                alertDialog.SetTitle(title);
                alertDialog.SetIcon(Android.Resource.Drawable.StatNotifyError);
                alertDialog.SetMessage(message);
                alertDialog.SetCancelable(false);
                alertDialog.SetButton("OK", OKButton);
                alertDialog.Show();

            }

         private void OKButton(object sender, DialogClickEventArgs e)
            {

            }

        
    }
}