﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;

namespace YooTask
{
    [Activity(Label = "@string/app_name", MainLauncher = true, LaunchMode = Android.Content.PM.LaunchMode.SingleTop, Icon = "@drawable/icon")]
    public class MainActivity : AppCompatActivity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //Set your main view here
           SetContentView(Resource.Layout.main);

            var Button = FindViewById<Button>(Resource.Id.button1);
            Button.Click += (s, e) =>
            {
                Intent nextActivity = new Intent(this, typeof(LoginActivity));
                StartActivity(nextActivity);
            };

            var Button2 = FindViewById<Button>(Resource.Id.button2);
            Button2.Click += (s, e) =>
            {
                Intent nextActivity = new Intent(this, typeof(RegiestrationActivity));
                StartActivity(nextActivity);
            };

        }
    }
}

