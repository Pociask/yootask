﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin;
using Android.Support.V7.App;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using YooTask.Model;
using System.Net;
using System.Threading.Tasks;
using System.Threading;

namespace YooTask
{
    [Activity(Label = "LoginActivit")]
    public class LoginActivity : AppCompatActivity
    {
        Button zaloguj;
        EditText editLogin, editPass;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Login);


            editLogin = FindViewById<EditText>(Resource.Id.editLogin);
            editPass = FindViewById<EditText>(Resource.Id.editPass);
            zaloguj = FindViewById<Button>(Resource.Id.Zaloguj);
            zaloguj.Click += Zaloguj_Click;
            var Button = FindViewById<Button>(Resource.Id.button1);
          
            // Create your application here

        }
        private async void Zaloguj_Click(object sender, EventArgs e)
        {

       
            PostContent post = new PostContent();
            HttpClient client = new HttpClient();
            post.email = editLogin.Text;
            post.password = editPass.Text;
            




           var uri = new Uri(string.Format($"http://192.168.134.66:8081/user/login?email=" + editLogin.Text + "&password=" + editPass.Text));
           // var uri = new Uri(string.Format("http://reqres.in/api/login?email=" + editLogin.Text + "&password=" + editPass.Text));
            HttpResponseMessage response;
            var json = JsonConvert.SerializeObject(post);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            response = await client.PostAsync(uri, content);
            
            
                if (response.StatusCode == HttpStatusCode.Accepted)
                {
                    var token = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                    {
                '"'

                    });
                    Toast.MakeText(this, token, ToastLength.Long).Show();
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                Intent nextActivity = new Intent(this, typeof(Menu));
                var token1 = JsonConvert.DeserializeObject(token);
                StartActivity(nextActivity);

            }
                else
                {
                    var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                    {
                '"'
                    });
                    Toast.MakeText(this, errorMessage1, ToastLength.Long).Show();
                Intent nextActivity = new Intent(this, typeof(Menu));
                var token1 = JsonConvert.DeserializeObject(errorMessage1);
                StartActivity(nextActivity);
            }
               // Intent nextActivity = new Intent(this, typeof(Menu));
               //StartActivity(nextActivity);
            }
           
        
         
    }
}


/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin;
using Android.Support.V7.App;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using YooTask.Model;
using System.Net;

namespace YooTask
{
    [Activity(Label = "LoginActivit")]
    public class LoginActivity : AppCompatActivity
    {
        Button zaloguj;
        EditText editLogin, editPass;

        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Login);


            editLogin = FindViewById<EditText>(Resource.Id.editLogin);
            editPass = FindViewById<EditText>(Resource.Id.editPass);
            zaloguj = FindViewById<Button>(Resource.Id.Zaloguj);
            zaloguj.Click += Zaloguj_Click;
            var Button = FindViewById<Button>(Resource.Id.button1);

            // Create your application here

        }
       /*  private async void Zaloguj_Click(object sender, EventArgs e)
          {


              PostContent post = new PostContent();
              HttpClient client = new HttpClient();
              post.email = editLogin.Text;
              post.password = editPass.Text;


              var uri = new Uri(string.Format($"http://192.168.134.66:8081/user/login?email=" + editLogin.Text + "&password=" + editPass.Text));
             //  var uri = new Uri(string.Format($"https://reqres.in/api/login?email=" + editLogin.Text + "&password=" + editPass.Text));
              HttpResponseMessage response;
              var json = JsonConvert.SerializeObject(post);
              var content = new StringContent(json, Encoding.UTF8, "application/json");

              client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
              response = await client.PostAsync(uri, content);

              if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
              {
                  var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                  {
                  '"'
                  });
                  Toast.MakeText(this, errorMessage1+"DOBRZE", ToastLength.Long).Show();
              }
              else
              {
                  var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                  {
                  '"'
                  });
                  Toast.MakeText(this, errorMessage1+"ZLE", ToastLength.Long).Show();
              }
              Intent nextActivity = new Intent(this, typeof(Menu));
              StartActivity(nextActivity);
          }*/
       /*private async void Zaloguj_Click(object sender, EventArgs e)
        {


            PostContent post = new PostContent();
            
            post.email = editLogin.Text;
            post.password = editPass.Text;

            var request = new HttpRequestMessage();
            request.RequestUri = new Uri("http://192.168.134.66:8081/user/login?email=" + editLogin.Text + "&password=" + editPass.Text);
            // var uri = new Uri(string.Format($"http://192.168.134.66:8081/user/login?email=" + editLogin.Text + "&password=" + editPass.Text));
            // var uri = new Uri(string.Format($"https://reqres.in/api/login?email=" + editLogin.Text + "&password=" + editPass.Text));
            request.Method = HttpMethod.Post;
            request.Headers.Add("Content-Type", "application/json");

            var client = new HttpClient();
          //  HttpResponseMessage response = await client.PostAsync(request.RequestUri, request);
          //  var json = JsonConvert.SerializeObject(post);
            //var content = new StringContent(json, Encoding.UTF8, "application/json");

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //response = await client.PostAsync(uri, content);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                {
                '"'
                });
                Toast.MakeText(this, errorMessage1 + "DOBRZE", ToastLength.Long).Show();
            }
            else
            {
                var errorMessage1 = response.Content.ReadAsStringAsync().Result.Replace("\\", "").Trim(new char[1]
                {
                '"'
                });
                Toast.MakeText(this, errorMessage1 + "ZLE", ToastLength.Long).Show();
            }
            HttpContent content = response.Content;
            var json = await content.ReadAsStringAsync();
          
            Intent nextActivity = new Intent(this, typeof(Menu));
        //StartActivity(nextActivity);
        //}
    }*/
//}

